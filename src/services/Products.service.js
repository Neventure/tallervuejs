import axios from "axios";

const baseURL = "http://170.239.85.65:4000";

export function productList() {
  return axios.get(`${baseURL}/products`);
}
